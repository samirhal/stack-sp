#pragma once

#include <iostream>
#include <string>

class AnnotatedString {
public:
  std::string value;
  int count;

  AnnotatedString() : count{objectCounter()} {
    std::cout << "default ctor (id: " << count << ")" << std::endl;
  }

  AnnotatedString(const char *s) : value{s}, count{objectCounter()} {
    std::cout << "value ctor (id: " << count << ")" << std::endl;
  }

  AnnotatedString(const AnnotatedString &as)
      : value{as.value}, count{objectCounter()} {
    std::cout << "copy ctor (id: " << count << ")"  << std::endl;
  }

  AnnotatedString(AnnotatedString &&as)
      : value{std::move(as.value)}, count{objectCounter()} {
    std::cout << "move ctor (id: " << count << ")"  << std::endl;
  }

  AnnotatedString &operator=(const AnnotatedString &as) {
    value = as.value;
    std::cout << "copy =" << std::endl;
    return *this;
  }
  AnnotatedString &operator=(AnnotatedString &&as) {
    value = std::move(as.value);
    std::cout << "move =" << std::endl;
    return *this;
  }
  ~AnnotatedString() {
    std::cout << "~dtor (id: " << count << ")" << std::endl;
  }

private:
  static int objectCounter() {
    static int counter = 0;
    return counter++;
  }
};
