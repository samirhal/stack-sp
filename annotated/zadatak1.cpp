#include <iostream>
#include <string>

#include "annotated.hpp"

class Customer {
  AnnotatedString firstname;
  AnnotatedString lastname;

 public:
  Customer(const AnnotatedString& ime, const AnnotatedString& prezime)
      : firstname{ime}, lastname{prezime} {}

  Customer(const char* ime, const char* prezime) {
    firstname = AnnotatedString{ime};
    lastname = AnnotatedString{prezime};
  }

  template <typename T>
  void setIme(T&& ime) {
    firstname = std::forward<T>(ime);
  }

  template <typename T>
  void setPrezime(T&& prezime) {
    lastname = std::forward<T>(prezime);
  }

};

int main() {
  // std::cout << "prvi primjer" << std::endl;
  /* Customer customer("Samir", "Halilcevic"); */

  /* std::cout << "primjer" << std::endl; */
  AnnotatedString ime{"Harun"};
  AnnotatedString prezime{"Delic"};
  Customer customer(ime, prezime);
  std::cout << "poziv settera I" << std::endl;
  customer.setIme(ime);
  std::cout << "kraj poziv settera I" << std::endl;
  std::cout << "poziv settera II" << std::endl;
  customer.setPrezime(std::move(prezime));
  std::cout << "kraj settera" << std::endl;

  return 0;
}

