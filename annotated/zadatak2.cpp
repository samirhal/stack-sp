#include <iostream>
#include <string>

#include "annotated.hpp"

class Customer {
  AnnotatedString firstname;
  AnnotatedString lastname;

 public:
  template<typename F, typename G>
  Customer(F&& ime, G&& prezime)
      : firstname{std::forward<F>(ime)}, lastname{std::forward<G>(prezime)} {}

  template <typename T>
  void setIme(T&& ime) {
    firstname = std::forward<T>(ime);
  }

  template <typename T>
  void setPrezime(T&& prezime) {
    lastname = std::forward<T>(prezime);
  }

};

int main() {
  AnnotatedString ime{"Harun"};
  AnnotatedString prezime{"Delic"};
  Customer customer(ime, std::move(prezime));
  return 0;
}

