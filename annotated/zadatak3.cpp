#include <iostream>
#include <string>

#include "annotated.hpp"

// copy elision
// rvo - return value optimisation
AnnotatedString stringFactory() {
  AnnotatedString ime;
  ime.value = "Mak";
  return ime;
}

int main() {
  auto ime = stringFactory();
  return 0;
}

