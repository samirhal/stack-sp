#pragma once

#include <stdexcept>

template <typename T>
class Queue {
 public:
  Queue() = default;
  Queue(const Queue& other);
  Queue(Queue&& other);
  Queue& operator=(const Queue& other);
  Queue& operator=(Queue&& other);
  ~Queue();

  void clear();

  size_t size() const {

  }
  
  bool empty() const {

  }

  void push(const T& element) {

  }

  void push(const T&& element) {

  }

  void pop() {

  }

  const T& top() const {

  }

  T& top() {

  }

 private:

};
