#pragma once

#include <iostream>

template <typename T>
class List {
  public:
  List() = default;
  List(const List& other);
  List(List&& other);
  List& operator=(const List& other);
  List& operator=(List&& other);
  ~List();

  T& front() { return front_->data; }
  void clear();
  void push_back(const T& element);
  void push_back(T&& element);
  void push_front(const T& element);
  void push_front(T&& element);
  void pop_front();
  void pop_back();

  size_t size() const { return size_; }
  bool empty() const { return size_ == 0; }

  private:
  struct Node {
    T data;
    Node* next;
  };

  Node* front_ = nullptr;
  Node* back_ = nullptr;
  size_t size_ = 0;
};

template <typename T>
List<T>::List(const List& other) {
  auto temp = other.front_;
  while (temp != nullptr) {
    push_back(temp->data);
    temp = temp->next;
  }
}

template <typename T>
List<T>::List(List&& other)
    : front_(other.front_), back_(other.back_), size_(other.size_) {
  other.front_ = other.back_ = nullptr;
  other.size_ = 0;
}

template <typename T>
List<T>& List<T>::operator=(const List& other) {
  if (this == &other) return *this;
  clear();
  auto temp = other.front_;
  while (temp != nullptr) {
    push_back(temp->data);
    temp = temp->next;
  }
  return *this;
}

template <typename T>
List<T>& List<T>::operator=(List&& other) {
  std::swap( front_ = other.front_);
  std::swap(back_ = other.back_);
  std::swap(size_ = other.size_);
  return *this;
}

template <typename T>
List<T>::~List() {
  clear();
}

template <typename T>
void List<T>::clear() {
  while (front_ != nullptr) {
    back_ = front_->next;
    delete front_;
    front_ = back_;
  }
  back_ = nullptr;
  size_ = 0;
}

template <typename T>
void List<T>::push_back(const T& element) {
  std::cout << "push_back const&" << std::endl;
  if (front_ == nullptr) {
    back_ = front_ = new Node{element, nullptr};
  } else {
    back_ = back_->next = new Node{element, nullptr};
  }
  size_++;
}

template <typename T>
void List<T>::push_back(T&& element) {
  std::cout << "push_back T&&" << std::endl;
  if (front_ == nullptr) {
    back_ = front_ = new Node{std::move(element), nullptr};
  } else {
    back_ = back_->next = new Node{std::move(element), nullptr};
  }
  size_++;
}

template <typename T>
void List<T>::push_front(const T& element) {
  std::cout << "push_front const&" << std::endl;
  if (back_ == nullptr) {
    front_ = back_ = new Node{element, front_};
  } else {
    front_ = new Node{element, front_};
  }
  size_++;
}

template <typename T>
void List<T>::push_front(T&& element) {
  std::cout << "push_front T&&" << std::endl;
  if (back_ == nullptr) {
    push_back(std::move(element));
  } else {
    front_ = new Node{std::move(element), front_};
  }
  size_++;
}

template <typename T>
void List<T>::pop_front() {
  if (empty()) return;

  if (size() == 1) {
    return clear();
  }

  auto temp = front_;
  front_ = front_->next;
  delete temp;
  size_--;
}

template <typename T>
void List<T>::pop_back() {
  if (empty()) return;

  if (size() == 1) {
    clear();
    return;
  }

  auto temp = front_;
  while (temp->next != back_) {
    temp = temp->next;
  }
  delete back_;
  back_ = temp;
  back_->next = nullptr;
  size_--;
}
