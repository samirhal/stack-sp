#pragma once

#include <cstddef>
#include "list.hpp"

template <typename T>
class Stack {
  public:

  size_t size() const {
    
  }
  bool empty() const {
    
  }
  T& top() {
    
  }

  const T& top() const { 
  }

  T pop() {
  }

  
  void push(T& element) {
    
  }

  void push(const T& element) {
    
  }

  private:
  List<T> impl;
};
